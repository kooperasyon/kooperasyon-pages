#!/bin/bash

# TODO: Implement this
# function notify {
#   if [ -x "$(command -v terminal-notifier)" ]; then
#     terminal-notifier -message "Site Deployed" -sound "Pop" -title "Brainchild Projects"
#   fi
# }

# If a command fails then the deploy stops
set -e

printf "\033[0;32mDeploying updates to Codeberg...\033[0m\n"

# Build the project.
hugo

# Go To Public folder
cd public

# Add changes to git.
git add .

# Commit changes.
msg="rebuilding site $(date)"
if [ -n "$*" ]; then
	msg="$*"
fi
git commit -m "$msg"

# Push source and build repos.
git push origin main

# notify
