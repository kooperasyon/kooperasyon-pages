---
title: "About this Blog"
date: 2022-07-04T22:38:43+08:00
draft: false
---

This is a collection of my research and notes on the state of worker
cooperatives in the Philippines. I wanted to know more about worker cooperatives
but found very few accessible resources about it within the context of the
Philippines.

<!--more-->

## Questions

On this blog, I hope to answer the following:

- What are examples of worker cooperatives in the Philippines and how well are
  they doing?
- How are worker cooperatives in the Philippines different from other countries
  like in the U.S., U.K., Italy, etc.?
- Why do Filipinos join or start worker co-ops?
- How can Filipinos start their own worker co-ops?
- What are the challenges faced by Filipino worker co-ops?
- How does the Philppine government help?
