---
title: "Style Guide"
date: 2022-07-06T07:42:36+08:00
draft: true
---

# Header 1

## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6

#tag

# Header

#tag

## Header

#tag

### Header

#tag

#### Header

#tag

##### Header

#tag

###### Header

#tag

## Underlined Head

#tag

# Double Underlined Head

#tag

Normal _Italics_ (\*) **Bold** (\*\*)

---

_Italics_ (\_) **Bold** (\_\_) %% Comment %% ~~Striked~~ ==Highlighted==

> Blockqoute

Tasks

- [ ] Task list unchecked.
- [x] Task list checked.

List Unordered (+)

- List item
- List item
- List item
  - Sub list item
  - Sub list item

List Unordered (-)

- List item
- List item
- List item
  - Sub list item
  - Sub list item

List Ordered

1. List item
   1. Sub list item
   2. Sub list item
      1. Sub sub list item
      2. Sub sub list item

List Unordered (\*)

- List item
- List item
- List item
  - Sub list item
  - Sub list item

[[WikiLink Page not created]] [[WikiLink Page created]]
[Link to website](http://www.site.cm)

Inline Math [$e^{2i\pi} = 1$]

Math Block $$\begin{vmatrix}a & b\\ c & d \end{vmatrix}=ad-bc$$

Code `inline`

Code block

```css
.selector {
property: value;
}
```

Image [[Some image.png]]

Embeded Image ![[Some image.jpg]]

Embeded Audio ![[Some audio.mp3]] Embeded Video ![[Some video.mp4]]

Embeded Note ![[Some note]]

This is a postponed footnote reference.[^1] This is inline footnote
reference.^[this is the inline note]

[^1]: this is the postponed note.

| Header | Header |
| ------ | ------ |
| Stub   | Cell   |

# Wombats

Wombats are short-legged, muscular quadrupedal marsupials that are native to
Australia. They are about 1 m (40 in) in length with small, stubby tails and
weigh between 20 and 35 kg (44 and 77 lb). There are three extant species and
they are all members of the family Vombatidae. They are adaptable and habitat
tolerant, and are found in forested, mountainous, and heathland areas of
southern and eastern Australia, including Tasmania, as well as an isolated patch
of about 300 ha (740 acres) in Epping Forest National Park in central
Queensland.

A peep at some distant orb has some power to raise and purify our thoughts like
a strain of sacred music, or a noble picture, or a passage from the grander
poets. “It always does one good, said Pedro.” I’ll teach you how to drive
quickly go there. You ought to know. The quick brown fox jumped over the lazy
dog! What?

1234567890 ABCDE FGHIJ KLMNO PQRST UVWXY Z

Though genetic studies of the Vombatidae have been undertaken, evolution of the
family is not well understood. Wombats are estimated to have diverged from other
Australian marsupials relatively early, as long as 40 million years ago, while
some estimates place divergence at around 25 million years. While some theories
place wombats as miniaturised relatives of diprotodonts, such as the
rhinoceros-sized Diprotodon, more recent studies place the Vombatiformes as
having a distinct parallel evolution, hence their current classification as a
separate family.

Wombats have an extraordinarily slow metabolism, taking around 8 to 14 days to
complete digestion, which aids their survival in arid conditions. They generally
move slowly.[citation needed] When threatened, however, they can reach up to 40
km/h (25 mph) and maintain that speed for 150 metres (490 ft). Wombats defend
home territories centred on their burrows, and they react aggressively to
intruders. The common wombat occupies a range of up to 23 ha (57 acres), while
the hairy-nosed species have much smaller ranges, of no more than 4 ha (10
acres).

Dingos and Tasmanian devils prey on wombats. Extinct predators were likely to
have included Thylacoleo and possibly the thylacine. Their primary defence is
their toughened rear hide, with most of the posterior made of cartilage. This,
combined with its lack of a meaningful tail, makes it difficult for any predator
that follows the wombat into its tunnel to bite and injure its target. When
attacked, wombats dive into a nearby tunnel, using their rumps to block a
pursuing attacker. A wombat may allow an intruder to force its head over the
wombat's back, and then use its powerful legs to crush the skull of the predator
against the roof of the tunnel, or drive it off with two-legged kicks, like
those of a donkey.

Wombats are generally quiet animals. Bare-nosed wombats can make a number of
different sounds, more than the hairy-nosed wombats. Wombats tend to be more
vocal during mating season. When angered, they can make hissing sounds. Their
call sounds somewhat like a pig's squeal. They can also make grunting noises, a
low growl, a hoarse cough, and a clicking noise.

## A secondary header

Paragraphs are separated by a blank line. This is a paragraph. _Italic_,
**bold**, and `monospace`. Itemized lists look like:

- this one

- that one

  - this is a nested item
  - so is this one

- the other one Note that --- not considering the asterisk --- the actual text
  content starts at 4-columns in.

  > Block quotes are written like so. They can span multiple paragraphs, if you
  > like. Use 3 dashes for an em-dash. Use 2 dashes for ranges (ex., "it's all
  > in chapters 12--14"). Three dots ... will be converted to an ellipsis.
  > Unicode is supported. ☺ An h2 header

---

### Here's a numbered list:

1. first item
2. second item
3. third item Note again how the actual text starts at 4 columns in (4
   characters from the left side). Here's a code sample:

## Let me re-iterate ...

for i in 1 .. 10 { do-something(i) } As you probably guessed, indented 4 spaces.
By the way, instead of indenting the block, you can use delimited blocks, if you
like:

```ruby
def foobar() {
  print "Welcome to flavor country!";
}
```

(which makes copying & pasting easier). You can optionally mark the delimited
block for Pandoc to syntax highlight it:

```python
import time
# Quick, count to ten!
for i in range(10):
# (but not *too* quick)
time.sleep(0.5)
print i
```

### An h3 header

Now a nested list:

First, get these ingredients:

- carrots
- celery
- lentils

5. Boil some water.

6. Dump everything in the pot and follow this algorithm:

       find wooden spoon
       uncover pot
       stir
       cover pot
       balance wooden spoon precariously on pot handle
       wait 10 minutes
       goto first step (or shut off burner when done)

   Do not bump wooden spoon or it will fall. Notice again how text always lines
   up on 4-space indents (including that last line which continues item 3
   above). Here's a link to [a website](http://foo.bar), to a
   [local doc](local-doc.html), and to a
   [section heading in the current doc](#an-h2-header). Here's a footnote [^1].

   [^1]: Footnote text goes here.

Tables can look like this:

| Size  | Material       | Color       |
| ----- | -------------- | ----------- |
| 9     | leather        | brown       |
| 10000 | hemp canvas is | natural     |
| 11    | glass          | transparent |
| 12    | rubber         | white       |
| 13    | rubber         | orange      |

(The above is the caption for the table.) Pandoc also supports multi-line
tables:

A horizontal rule follows.

---

Here's a definition list: apples : Good for making applesauce. oranges : Citrus!
tomatoes : There's no "e" in tomatoe. Again, text is indented 4 spaces. (Put a
blank line between each term/definition pair to spread things out more.) Here's
a "line block": | Line one | Line too | Line tree and images can be specified
like so:

Inline math equations go in like so: $\omega = d\phi / dt$. Display math should
get its own line and be put in in double-dollarsigns: $$I = \int \rho R^{2} dV$$
And note that you can backslash-escape any punctuation characters which you wish
to be displayed literally, ex.: \`foo\`, \*bar\*, etc.
