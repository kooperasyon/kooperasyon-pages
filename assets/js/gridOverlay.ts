function findElement(selector) {
  return document.querySelector(selector);
}

function ready(fn) {
  if (
    document.readyState ?? document.readyState !== "loading"
  ) {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

function onKeypress(key, fn) {
  document.addEventListener("keypress", function (e) {
    console.log(e);
    if (e.isTrusted && key.key === e.key && e.ctrlKey === e.ctrlKey) {
      fn(e);
    }
  });
}

function dasherize(string) {
  return string.replace(/([A-Z])/g, g => `-${g[0].toLowerCase()}`);
}

function style(element, styles) {
  element.style.cssText = Object.entries(styles)
    .map(([prop, value]) => {
      return `${dasherize(prop)}:${value}`;
    })
    .join(";");
}

class OverlayElement {
  containerSelector: string;

  constructor(containerSelector) {
    this.containerSelector = containerSelector;
  }

  listen() {
    console.log("listening...");
    ready(() => {
      console.log("ready done");
      onKeypress({ key: "g", ctrlKey: true }, () => {
        this.toggleOverlay();
      });
    });
  }

  container() {
    return findElement(this.containerSelector);
  }

  overlay() {
    return findElement(`${this.containerSelector} .grid-overlay`);
  }

  showOverlay() {
    if (this.overlay()) {
      this.overlay().style.display = "block";
    } else {
      this.createOverlay();
    }
  }

  hideOverlay() {
    if (this.overlay()) {
      this.overlay().style.display = "none";
    }
  }

  toggleOverlay() {
    if (this.overlayIsVisible()) {
      this.hideOverlay();
    } else {
      this.showOverlay();
    }
  }

  overlayIsVisible() {
    const overlay = this.overlay();
    return overlay && overlay.style.display !== "none";
  }

  createOverlay() {
    const overlay = document.createElement("div");
    const container = this.container();
    overlay.classList.add("grid-overlay");
    container.style.position = "relative";
    container.appendChild(overlay);
  }
}

export default OverlayElement;
